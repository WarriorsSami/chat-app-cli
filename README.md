# chat-app-cli
Basic Chat App designed for CLI in Rustlang and, respectively, Golang (the former using TCP Listener and Stream and the latter using gRPC and Docker).


### CLI Chat App - Rustlang implementation
![rust-gif](https://github.com/WarriorsSami/chat-app-cli/blob/master/rustlang/assets/cli-chat-app.gif)
